import justpy as jp
import pandas as pd
from datetime import datetime
from pytz import utc
import matplotlib.pyplot as plt

df = pd.read_csv("reviews.csv", parse_dates=['Timestamp'])

df['Month'] = df['Timestamp'].dt.strftime('%Y-%m')
res_month = df.groupby(['Month','Course Name'])['Rating'].mean().unstack()


chart_def = """
{
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Moose and deer hunting in Norway, 2000 - 2021',
        align: 'left'
    },
    subtitle: {
        text: 'Source: <a href="https://www.ssb.no/jord-skog-jakt-og-fiskeri/jakt" target="_blank">SSB</a>',
        align: 'left'
    },
    legend: {

    },
    xAxis: {
        categories: [
            'aaa',
            'bbb',
            'ccc'
            ],
        plotBands: [{ // Highlight the two last years
            from: 2019,
            to: 2020,
            color: 'rgba(68, 170, 213, .2)'
        }]
    },
    yAxis: {
        title: {
            text: 'Quantity'
        }
    },
    tooltip: {
        shared: true,
        headerFormat: '<b>Hunting season starting autumn {point.x}</b><br>'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        series: {
            pointStart: 2000
        },
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Moose',
        data:
            [
                38000,
                37300,
                37892,
                38564,
                36770,
                36026,
                34978,
                35657,
                35620,
                35971,
                36409,
                36435,
                34643,
                34956,
                33199,
                31136,
                30835,
                31611,
                30666,
                30319,
                31766
            ]
    }, {
        name: 'Deer',
        data:
            [
                22534,
                23599,
                24533,
                25195,
                25896,
                27635,
                29173,
                32646,
                35686,
                37709,
                39143,
                36829,
                35031,
                36202,
                35140,
                33718,
                37773,
                42556,
                43820,
                46445,
                50048
            ]
    }]
}
"""

def app():
    wp = jp.QuasarPage()


    hc = jp.HighCharts(a=wp, options=chart_def)
    hc.options.xAxis.categories = list(res_month.index)
    hc.options.title.text = "Average Rating by Course - All Months"
    hc.options.title.align = 'center'
    hc_data = [{"name":v1, "data":[v2 for v2 in res_month[v1]]} for v1 in res_month.columns]
    hc.options.series = hc_data
    hc.options.yAxis.title.text = "Rating Week"


    return wp
    
jp.justpy(app)