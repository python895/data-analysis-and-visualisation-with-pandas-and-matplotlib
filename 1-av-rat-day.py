import justpy as jp
import pandas as pd
from datetime import datetime
from pytz import utc
import matplotlib.pyplot as plt

df = pd.read_csv("reviews.csv", parse_dates=['Timestamp'])
df['Day'] = df['Timestamp'].dt.date
avg_day = df.groupby(['Day']).mean()

chart_def = """
{
    chart: {
        type: 'spline',
        inverted: false
    },
    title: {
        text: 'Atmosphere Temperature by Altitude',
        align: 'center'
    },
    subtitle: {
        text: '',
        align: 'left'
    },
    xAxis: {
        reversed: false,
        title: {
            enabled: true,
            text: 'Altitude'
        },
        labels: {
            format: '{value}'
        },
        accessibility: {
            rangeDescription: 'Range: 0 to 80 km.'
        },
        maxPadding: 0.05,
        showLastLabel: true
    },
    yAxis: {
        title: {
            text: 'Average Rating'
        },
        labels: {
            format: '{value}'
        },
        accessibility: {
            rangeDescription: 'Range: -90°C to 20°C.'
        },
        lineWidth: 2
    },
    legend: {
        enabled: false
    },
    tooltip: {
        headerFormat: '<b>{series.name}</b><br/>',
        pointFormat: '{point.x} Day: {point.y}'
    },
    plotOptions: {
        spline: {
            marker: {
                enable: false
            }
        }
    },
    series: [{
        name: 'Temperature',
        data: [[0, 15], [10, -50], [20, -56.5], [30, -46.5], [40, -22.1],
            [50, -2.5], [60, -27.7], [70, -55.7], [80, -76.5]]
    }]
}
"""

def app():
    wp = jp.QuasarPage()

    hc = jp.HighCharts(a=wp, options=chart_def)

    hc.options.title.text = "Average Rating by Day"
    hc.options.title.align = "center"
    hc.options.xAxis.categories = list(avg_day.index)
    hc.options.yAxis.title.text = "Average Rating"
    hc.options.xAxis.title.text = "Date"
    hc.options.yAxis.labels.format = ""
    hc.options.series[0].data = list(avg_day['Rating'])
    hc.options.series[0].name = "Average Rating"
    
    return wp
    
jp.justpy(app)
